/* Simple WiFi Sign

   This sign code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "rom/ets_sys.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_https_ota.h"
#include "nvs_flash.h"
#include "esp8266/gpio_struct.h"
#include "esp8266/pin_mux_register.h"
#include "ws2812_i2s.h"
#include "rainbow.h"

extern const uint8_t gitlab_cert_pem_start[] asm("_binary_gitlab_ca_cert_pem_start");
extern const uint8_t gitlab_cert_pem_end[] asm("_binary_gitlab_ca_cert_pem_end");

static char received_etag[64] = {0};

/* The sign use simple WiFi configuration that you can set via
   'make menuconfig'.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define SIGN_WIFI_SSID "mywifissid"
*/
#define SIGN_ESP_WIFI_SSID      CONFIG_ESP_WIFI_SSID
#define SIGN_ESP_WIFI_PASS      CONFIG_ESP_WIFI_PASSWORD

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int WIFI_CONNECTED_BIT = BIT0;

static const char *TAG = "hotpotato sign";
static const char *OTA_TAG = "ota";

#define NUMLEDS 28

int8_t led_x[NUMLEDS] = {
  -3,-1,1,3,
  5,5,5,5,
  5,5,5,5,
  3,1,-1,-3,
  -5,-5,-5,-5,
  -5,-5,-5,-5,
  -3,-1,1,3,
};

int8_t led_y[NUMLEDS] = {
  8,8,8,8,
  7,5,3,1,
  -1,-3,-5,-7,
  -8,-8,-8,-8,
  -7,-5,-3,-1,
  1,3,5,7,
  0,0,0,0
};

uint8_t segs[] = {
  0b1111110, // 0
  0b0110000, // 1
  0b1101101, // 2
  0b1111001, // 3
  0b0110011, // 4
  0b1011011, // 5
  0b1011111, // 6
  0b1110000, // 7
  0b1111111, // 8
  0b1110011, // 9
  0b1110111, // A
  0b0011111, // B
  0b1001110, // C
  0b0111101, // D
  0b1001111, // E
  0b1000111, // F
  0b0000001  // -
};

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
  switch(evt->event_id) {
  case HTTP_EVENT_ERROR:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_ERROR");
    break;
  case HTTP_EVENT_ON_CONNECTED:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_ON_CONNECTED");
    break;
  case HTTP_EVENT_HEADER_SENT:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_HEADER_SENT");
    break;
  case HTTP_EVENT_ON_HEADER:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
    if (strncasecmp(evt->header_key, "etag", 4) == 0) {
      ESP_LOGI(OTA_TAG, "New etag: %s", evt->header_value);
      strncpy(received_etag, evt->header_value, 64);
    }
    break;
  case HTTP_EVENT_ON_DATA:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    break;
  case HTTP_EVENT_ON_FINISH:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_ON_FINISH");
    break;
  case HTTP_EVENT_DISCONNECTED:
    ESP_LOGD(OTA_TAG, "HTTP_EVENT_DISCONNECTED");
    break;
  }
  return ESP_OK;
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        xEventGroupSetBits(wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:
        ESP_LOGI(TAG, "station:"MACSTR" join, AID=%d",
                 MAC2STR(event->event_info.sta_connected.mac),
                 event->event_info.sta_connected.aid);
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
        ESP_LOGI(TAG, "station:"MACSTR"leave, AID=%d",
                 MAC2STR(event->event_info.sta_disconnected.mac),
                 event->event_info.sta_disconnected.aid);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}


void wifi_init_sta()
{
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = SIGN_ESP_WIFI_SSID,
            .password = SIGN_ESP_WIFI_PASS
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    ESP_LOGI(TAG, "connect to ap SSID:%s password:%s",
             SIGN_ESP_WIFI_SSID, SIGN_ESP_WIFI_PASS);
}

// Info on reading cycle counter from https://github.com/kbeckmann/nodemcu-firmware/blob/ws2812-dual/app/modules/ws2812.c
__attribute__ ((always_inline)) inline static uint32_t __clock_cycles() {
  uint32_t cyc;
  __asm__ __volatile__ ("rsr %0,ccount":"=a" (cyc));
  return cyc;
}

// #define PIN_MASK (1 << 5)
#define PIN_MASK (1 << 3)


uint8_t led_data[512] = {0};

void render_task(void * pvParameter)
{
  TickType_t lastWakeTime = xTaskGetTickCount();
  uint8_t num = 0;
  uint32_t framecount = 0;

  while(1) {
    vTaskDelayUntil(&lastWakeTime, 2);
    /* ESP_LOGI(TAG, "Rendering %d", num); */
    // Confirmed clock_cyles increments at 80MHz

    int8_t dx = 0;
    int8_t dy = 4;

    for (int led = 0; led < NUMLEDS; led++) {
      // Radial
      /* float dist = sqrtf((led_x[led]+dx)*(led_x[led]+dx) + (led_y[led]+dy)*(led_y[led]+dy))/8.7f; */

      // Y-axis
      float dist = (led_y[led]-8)*0.05;

      uint8_t on = (segs[num] >> (6 - (led >> 2))) & 1;

      float theta = fmodf(0.5*dist + framecount*0.001, 1.0f);

      struct RGB rgb = HSLToRGB((struct HSL) {.H = theta, .S = 0.99, .L = 0.5});

      led_data[led*3+0] = (uint8_t) ((rgb.G >> 8) * ((uint16_t) 0xFF) * on) / (uint8_t) 2;
      led_data[led*3+1] = (rgb.R >> 8) * ((uint16_t) 0xFF) * on;
      led_data[led*3+2] = (uint8_t) ((rgb.B >> 8) * ((uint16_t) 0xFF) * on) / (uint8_t) 2;
    }

    ws2812_push(led_data, 3*NUMLEDS);

    if (framecount % 100 == 0) {
      num++;
      num %= 17;
    }

    framecount++;
  }
}

void attempt_ota_task(void * pvParameter)
{
  // TODO: heck, gitlab changes the etag every time :/
  // Rethink all this -- maybe use the gitlab api? That means parsing json
  esp_err_t ret;

  ESP_LOGI(OTA_TAG, "Starting OTA");

  nvs_handle nvs_ns;
  ret = nvs_open("sign-ota", NVS_READWRITE, &nvs_ns);
  ESP_ERROR_CHECK(ret);

  size_t required_size;
  char* etag = NULL;
  ret = nvs_get_str(nvs_ns, "etag", NULL, &required_size);
  if (ret == ESP_ERR_NVS_NOT_FOUND) {
    ESP_LOGI(OTA_TAG, "No current ETag, will attempt upgrade unconditionally");
  } else {
    etag = malloc(required_size);
    nvs_get_str(nvs_ns, "etag", etag, &required_size);
    ESP_LOGI(OTA_TAG, "Current ETag is %s", etag);
  }

  /* Wait for wifi to come online. */
  xEventGroupWaitBits(wifi_event_group, WIFI_CONNECTED_BIT,
                      false, true, portMAX_DELAY);
  ESP_LOGI(OTA_TAG, "Connected to Wifi! Try to get etag...");

  esp_http_client_config_t config = {
    .url = CONFIG_OTA_ARTIFACT_URL,
    .cert_pem = (char *)gitlab_cert_pem_start,
  };

  esp_http_client_handle_t client = esp_http_client_init(&config);
  if (etag)
    esp_http_client_set_header(client, "If-None-Match", etag);

  ret = esp_http_client_open(client, 0);
  ESP_ERROR_CHECK(ret);
  assert(esp_http_client_fetch_headers(client) != -1);
  int statuscode = esp_http_client_get_status_code(client);

  if (statuscode == 304) {
    ESP_LOGI(OTA_TAG, "304 Not Modified, no ota necessary!");
  } else if (statuscode == 200) {
    ESP_LOGI(OTA_TAG, "200 OK, let's ota!");
  }
  esp_http_client_close(client);
  esp_http_client_cleanup(client);
  free(etag);

  config.event_handler = _http_event_handler;


  ret = esp_https_ota(&config);
  if (ret == ESP_OK) {
    if (received_etag[0] == '\0') {
      ESP_LOGW(OTA_TAG, "No etag in response! Will be in ota loop :(");
    } else {
      nvs_set_str(nvs_ns, "etag", received_etag);
      ESP_LOGI(OTA_TAG, "Updated etag: %s", received_etag);
    }
    ESP_LOGI(OTA_TAG, "Restarting for OTA :3");
    esp_restart();
  } else {
    ESP_LOGE(OTA_TAG, "Firmware Upgrades Failed");
  }
  while (1) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}


void app_main()
{
  //Initialize NVS
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
  wifi_init_sta();

  ws2812_init();

  /* for (int led = 0; led <= NUMLEDS; led++) { */
  /*   if ((led >= 4 && led <= 11) || led == 24 || led == 25) { */
  /*     led_data[led*3+0] = 0x22; */
  /*     led_data[led*3+1] = 0xFF; */
  /*     led_data[led*3+2] = 0x88; */
  /*   } */
  /* } */

  ws2812_push(led_data, 3*NUMLEDS);


  /* xTaskCreate(&attempt_ota_task, "ota_task", 8192, NULL, 5, NULL); */
  xTaskCreate(&render_task, "render_task", 8192, NULL, 8, NULL);
}
